from django import forms
# from django.forms import ModelForm
# from receipts.models import Receipt, Account, ExpenseCategory


class Login(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        widget= forms.PasswordInput,
    )


class CreateUser(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        widget= forms.PasswordInput,
    )
    password_confirmation = forms.CharField(
        max_length=150,
        widget= forms.PasswordInput,
    )