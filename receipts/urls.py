from django.urls import path
from receipts.views import show_all_receipt, new_receipt, account_list, category_list, add_category, add_account

urlpatterns = [
    path("", show_all_receipt, name='home'),
    path('create/', new_receipt, name='create_receipt'),
    path('accounts/', account_list, name='account_list'),
    path('categories/', category_list, name='category_list'),
    path('categories/create/', add_category, name='create_category'),
    path('accounts/create/', add_account, name='create_account'),
]



