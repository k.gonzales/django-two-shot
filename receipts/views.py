from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from receipts.models import Account, Receipt,ExpenseCategory
from receipts.forms import AddReceipt, AddExpenseCategory, AddNewAccount

# Create your views here.
@login_required
def show_all_receipt(request):
    receipt = Receipt.objects.filter(purchaser= request.user)
    context = {
        'receipt': receipt,
    }
    return render(request, 'list.html', context)

@login_required
def new_receipt(request):
    if request.method == "POST":
        form = AddReceipt(request.POST)
        if form.is_valid():
            receipt= form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect('home')
    else:
        form = AddReceipt()
    context = {
        'form':form,
    }
    return render(request, 'create_receipt.html', context)

@login_required
def category_list(request):
    category = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        'category':category,
    }
    return render(request, 'category_lst.html', context)

@login_required
def account_list(request):
    account = Account.objects.filter(owner= request.user)
    context = {
        'account':account,
    }
    return render(request, 'account_lst.html', context)

@login_required
def add_category(request):
    if request.method == 'POST':
        form = AddExpenseCategory(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect('category_list')
    else:
        form = AddExpenseCategory()
    context = {
        'form': form,
    }
    return render(request, 'add_category.html', context)

@login_required
def add_account(request):
    if request.method == 'POST':
        form = AddNewAccount(request.POST)
        if form.is_valid():
            account= form.save(False)
            account.owner = request.user
            account.save()
            return redirect('account_list')
    else:
        form = AddNewAccount()
    context = {
        'form' : form,
    }
    return render(request, 'add_account.html', context)